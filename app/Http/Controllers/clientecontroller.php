<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class clientecontroller extends Controller
{
    
     public function iniciocliente(Request $request)
     {
        $cliente =cliente::all();
        return view('cliente.inicio')->with('cliente',$cliente);
     }
     
    public function crearcliente(Request $request)
    {
        $cliente - cliente::all();
         return view('cliente.crear')->with('cliente', $cliente);
    }

    public function guardarcliente(Request $request){
        $this->validate($request,[
            'nombre'=>'required',
            'apellido'=>'required',
            'cedula'=>'required',
            'direccion'=>'required',
            'telefono'=>'required',
            'fecha_nacimiento'=>'required',
            'email'=>'required'
        ]);

        $cliente = new cliente;
        $cliente ->nombre=$request->nombre;
        $cliente->apellido=$request->apellido;
        $cliente->cedula=$request->cedula;
        $cliente->direccion=$request->direccion;
        $cliente->telefono=$request->telefono;
        $cliente->fecha_nacimiento=$request->fecha_nacimiento;
        $cliente->email=$request->email;
        $cliente->save();
        return redirect()->route('list.clientes');
    }

}
