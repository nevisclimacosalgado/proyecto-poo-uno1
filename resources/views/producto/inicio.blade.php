@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE PRODUCTO</div>

                <div class="col text right">
                     <a href=""class="btn btn-sm tbm-primary">Nuevo producto</a>
                     </div>

                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col"># ID</th>
                                    <th scope="col"># nombre</th>
                                    <th scope="col"># tipo</th>
                                    <th scope="col"># estado</th>
                                    <th scope="col"># precio</th>
                                   </tr>
                                   </thead>
                                    <tbody>
                                        @foreach ($producto as $item)
                                        <tr>
                                            <th scope="row">{{$item->id}}</th>
                                            <td >{{$item->nombre}}</td>
                                            <td >{{$item->tipo}}</td>
                                            <td >{{$item->estado}}</td>
                                            <td >{{$item->precio}}</td>
                                            </tr>
                                            @endforeach

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
