<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' =>['auth']], function() {  
route::get('/lista/productos',['as'=>'list.producto', 'uses'=>'productocontroler@inicioproducto']);
route::get('/lista/productos',['as'=>'crear.producto', 'uses'=>'productocontroler@crearproducto']);
route::post('/guardar/productos',['as'=>'guardar.producto', 'uses'=>'productocontroler@guardarproducto']);
route::get('/lista/cliente',['as'=>'list.cliente', 'uses'=>'clientecontroler@iniciocliente']);
route::get('/crear/cliente',['as'=>'crear.cliente', 'uses'=>'clientecontroler@crearcliente']);
route::post('/guardar/cliente',['as'=>'guardar.cliente', 'uses'=>'clientecontroler@guardarcliente']);


});
